<?php
$array = [2, 6, 19, 14, 13];
$answer = NULL;
$raznost = $array[1] - $array[0];

for ($i = 2; $i < count($array); $i++)
{
    if ($array[$i] - $array[$i - 1] != $raznost)
    {
        $answer = false;
    } else {
        $answer = true;
    }
}

if ($answer == true)
{
    print $raznost;
}
else {
    print "NULL";
}